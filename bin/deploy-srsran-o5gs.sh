set -ex
COMMIT_HASH=$1
BINDIR=`dirname $0`
ETCDIR=/local/repository/etc
source $BINDIR/common.sh

if [ -f $SRCDIR/srs-o5gs-setup-complete ]; then
  echo "setup already ran; not running again"
  exit 0
fi

# use latest UHD from Ettus PPA; 4.6.0 as of 06/2024.
# there seems to be an issue with 4.4 that affects the x310.
sudo add-apt-repository -y ppa:ettusresearch/uhd
sudo apt-get update
sudo apt-get install -y libuhd-dev uhd-host
sudo uhd_images_downloader -tb2
sudo uhd_images_downloader -tx3

# srsran dependencies
sudo apt-get install -y \
  cmake \
  make \
  gcc \
  g++ \
  iperf3 \
  pkg-config \
  libboost-dev \
  libfftw3-dev \
  libmbedtls-dev \
  libsctp-dev \
  libyaml-cpp-dev \
  libgtest-dev \
  numactl \
  ppp

# docker and wireshark/tshark
sudo add-apt-repository -y ppa:wireshark-dev/stable
echo "wireshark-common wireshark-common/install-setuid boolean false" | sudo debconf-set-selections
sudo apt update

sudo apt-get install -y \
  docker \
  docker-compose \
  software-properties-common \
  tshark \
  wireshark

cd $SRCDIR
git clone $SRS_PROJECT_REPO
cd srsRAN_Project
git checkout $COMMIT_HASH
git apply $ETCDIR/srsran/powder-srs.patch
mkdir build
cd build
cmake ../
make -j $(nproc)

echo configuring nodeb...
mkdir -p $SRCDIR/etc/srsran
cp -r $ETCDIR/srsran/* $SRCDIR/etc/srsran/
echo configuring nodeb... done.

echo configuring and starting open5gs container...
cp $ETCDIR/open5gs/subscriber_db.csv $SRCDIR/srsRAN_Project/docker/open5gs/
cd $SRCDIR/srsRAN_Project/docker/open5gs
sudo docker network create --subnet=10.53.1.0/16 open5gsnet
sudo docker build --target open5gs -t open5gs-docker .
sudo docker run --net open5gsnet --ip 10.53.1.2 --env-file open5gs.env --privileged --publish 9999:9999 -d open5gs-docker ./build/tests/app/5gc -c open5gs-5gc.yml
echo configuring and starting open5gs container... done.

touch $SRCDIR/srs-o5gs-setup-complete
