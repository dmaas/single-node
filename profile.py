import os
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.urn as URN
import geni.rspec.igext as IG
import geni.rspec.emulab.pnext as PN
import geni.rspec.emulab as emulab

tourDescription = """

## Profile for deploying a single node

"""
tourInstructions = ""


class GLOBALS(object):
    BIN_PATH = "/local/repository/bin"
    DEFAULT_COMP_MANAGER_ID = "urn:publicid:IDN+emulab.net+authority+cm"
    DEFAULT_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
    DEFAULT_SRSRAN_HASH = "a15950301c5f3a1a166b79bb6c9ee901a4e8c2dd" # release_24_04
    SRS_O5GS_DEPLOY_SCRIPT = os.path.join(BIN_PATH, "deploy-srsran-o5gs.sh")


pc = portal.Context()
pc.defineParameter(
    name="node_type",
    description="hardware type to use",
    typ=portal.ParameterType.STRING,
    defaultValue=""
)
pc.defineParameter(
    name="component_id",
    description="component ID to use",
    typ=portal.ParameterType.STRING,
    defaultValue=""
)
pc.defineParameter(
    name="component_manager_id",
    description="component manager ID to use",
    typ=portal.ParameterType.STRING,
    defaultValue=GLOBALS.DEFAULT_COMP_MANAGER_ID
)
pc.defineParameter(
    name="disk_image",
    description="disk image to use",
    typ=portal.ParameterType.STRING,
    defaultValue=GLOBALS.DEFAULT_IMG
)
pc.defineParameter(
    name="rf_radiated",
    description="include rf radiated desire",
    typ=portal.ParameterType.BOOLEAN,
    defaultValue=False
)
pc.defineParameter(
    name="start_vnc",
    description="enable noVNC",
    typ=portal.ParameterType.BOOLEAN,
    defaultValue=False
)
pc.defineParameter(
    name="remote_data_source",
    description="connect a remote data source",
    typ=portal.ParameterType.STRING,
    defaultValue=""
)
pc.defineParameter(
    name="deploy_o5gs_srs",
    description="deploy open5gs and srsRAN project",
    typ=portal.ParameterType.BOOLEAN,
    defaultValue=False,
    advanced=True
)
pc.defineParameter(
    name="srsran_commit_hash",
    description="commit hash for srsRAN",
    typ=portal.ParameterType.STRING,
    defaultValue="",
    advanced=True
)

params = pc.bindParameters()
pc.verifyParameters()
request = pc.makeRequestRSpec()

node = request.RawPC("node")
node.disk_image = params.disk_image

if params.node_type:
    node.hardware_type = params.node_type
else:
    node.component_id = params.component_id
    node.component_manager_id = params.component_manager_id

if params.rf_radiated:
    node.Desire("rf-radiated", 1.0)


if params.remote_data_source:
    iface = node.addInterface()
    fsnode = request.RemoteBlockstore("fsnode", "/data")
    fsnode.dataset = params.remote_data_source
    fslink = request.Link("fslink")
    fslink.addInterface(iface)
    fslink.addInterface(fsnode.interface)
    fslink.best_effort = True
    fslink.vlan_tagging = True

if params.deploy_o5gs_srs:
    if params.srsran_commit_hash:
        srsran_hash = params.srsran_commit_hash
    else:
        srsran_hash = GLOBALS.DEFAULT_SRSRAN_HASH
    cmd = "{} '{}'".format(GLOBALS.SRS_O5GS_DEPLOY_SCRIPT, srsran_hash)
    node.addService(rspec.Execute(shell="bash", command=cmd))

if params.start_vnc:
    node.startVNC()

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

pc.printRequestRSpec(request)
